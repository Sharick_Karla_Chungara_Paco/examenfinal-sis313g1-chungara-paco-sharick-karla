"use client"


import Image from "next/image";
import styles from "./page.module.css";
import { Box, Grid, Typography, ThemeProvider, Button, TextField } from "@mui/material";
import { themePersonal } from '../helpers/theme';
import { BoxInfo } from "@/components/BoxInfo";
import BoxTrend from "@/components/BoxTrend";
import { Search } from "@mui/icons-material";
import { BoxStarted } from "@/components/BoxStarted";
import BoxEnd from "@/components/BoxEnd";
export default function Home() {
  return (

  
      <Box sx={{ display: "flex"}}>
        <Box component="main" sx={{ p: 3, width: "100%" }}>
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <Box sx={{ display: "flex", justifyContent: "center", flexDirection: "column" }}>
              <Typography sx={{ fontSixe: "36px", fontWeight: "700", color: "#ECF1F0" }} variant="h3" gutterBottom>
                Start and Build Your Crypto Portfolio Here
              </Typography>

              <Typography sx={{ fontSize: "18px", margin: "50px", color: "#B6B6B6", width: "500px", height: "58px", justifyContent: "center" }} variant="body2" gutterBottom>
                Only at CryptoCap, you can build a good portfolio and learn best practices about cryptocurrency.

              </Typography>


              <Box sx={{ display: "flex", justifyContent: "center" }}>
                <Button sx={{ backgroundColor: "#0FAE96", width: "190px", height: "56px", borderRadius: "10px", color: "white" }} variant="contained" > Get Started </Button>

              </Box>

            </Box>

          </Box>
          <Grid container spacing={2} sx={{ marginY: "40px" }}>
            <Grid item xs={12}>
              <Typography sx={{ color: "#ECF1F0", fontWeight: "600", fontSize: "24px" }} variant="h5" gutterBottom className="pagination-item">
                Market Trend
              </Typography>
              <Box sx={{ display: "flex", justifyContent: "space-between" }}>
                <BoxTrend imgUrl="/images/market4.svg" title="BTN" button=" BITCOIN" precio="$56,623.54" porcentaje="1.41%"></BoxTrend>
                <BoxTrend imgUrl="/images/market2.svg" title="ETN" button=" ETHEREUM" precio="$4,267.90" porcentaje="1.41%"></BoxTrend>
                <BoxTrend imgUrl="/images/market3.svg" title="BNB" button=" BINANCE"precio="$587.74" porcentaje="0.82%"></BoxTrend>
                <BoxTrend imgUrl="/images/market.svg" title="USDT" button="USDT" precio="$0.99982" porcentaje="0,03%"></BoxTrend>
              </Box>
            </Grid>

          </Grid>

          <Grid container spacing={2} sx={{ marginY: "40px" }}>
            <Grid item xs={12}>
              <Box sx={{ display: "flex", justifyContent: "center" }}>
                <Box sx={{ display: "flex", justifyContent: "center", flexDirection: "column" }}>
                  <Typography sx={{ color: "white" }} variant="h3" gutterBottom className="pagination-item">
                    CryptoCap Amazing Faetures
                  </Typography>
                  <Box sx={{ width: "1256px", height: "41px", marginBottom: "50px" }}>
                    <Typography sx={{ fontSize: "18px", color: "#B6B6B6", justifyContent: "center" }} variant="body2" gutterBottom>
                      Explore sensational features to prepare your best investment in cryptocurrency
                    </Typography>
                  </Box>
                </Box>
              </Box>
              <Box sx={{ display: "flex", justifyContent: "space-between" }}>"
                <BoxInfo image="/images/Icon1.svg" title="Manage Portfolio" parrafo="Buy and sell popular digital currencies, keep track of them in the one place"></BoxInfo>
                <BoxInfo image="/images/Icon2.svg" title="Manage Portfolio" parrafo="Buy and sell popular digital currencies, keep track of them in the one place."></BoxInfo>
                <BoxInfo image="/images/Icon3.svg" title="Manage Portfolio" parrafo="Buy and sell popular digital currencies, keep track of them in the one place."></BoxInfo>
                <BoxInfo image="/images/Icon4.svg" title="Manage Portfolio" parrafo="Buy and sell popular digital currencies, keep track of them in the one place."></BoxInfo>

              </Box>
            </Grid>

          </Grid>

          <Box sx={{ widht: "1259px", height: "142px", display: "flex", border: '1px solid #FFFFFF0D', padding: "20px 20px 50px 50px" }}>
            <Box>
              <Typography sx={{ color: "#ECF1F0", fontSize: "24px" }} variant="h4" gutterBottom>
                New In Cryptocurrency?
              </Typography>
              <Typography sx={{ color: "#B6B6B6" }} variant="body2" gutterBottom>
                We'll tell you what cryptocurrencies are, how they work and why <br /> you should own one right now. So let's do it.
              </Typography>
            </Box>
            <Box sx={{ marginLeft: "750px", marginTop: "20px" }}>
              <Button sx={{ backgroundColor: "#0FAE96", color: "#FFFFFF", width: "207px", height: "56px", borderRadius: "10px", fontSize: "12px" }} variant="contained">Learn & Explore Now</Button>
            </Box>
          </Box>

          <Box>
            <Typography sx={{ color: "#ECF1F0", fontWeight: "600", fontSize: "24px", marginTop: "50px" }} variant="h5" gutterBottom className="pagination-item">
              Market Update
            </Typography>
            <Typography sx={{ color: "#ECF1F0", fontWeight: "500", fontSize: "18px" }} variant="caption" display="block" gutterBottom>
              Cryptocurrency Categories
            </Typography>
            <Box sx={{ display: "flex", flexDirection: "row" }}>
              <Box >
                <Button className="Botones" sx={{ backgroundColor: "#FFFFFF0D", color: "#B6B6B6" }} variant="contained">Popular</Button>
                <Button className="Botones" sx={{ backgroundColor: "#FFFFFF0D", color: "#B6B6B6" }} variant="contained">Metaverse</Button>
                <Button className="Botones btn1" sx={{ backgroundColor: "#FFFFFF0D", color: "#B6B6B6" , width: "60px" }} variant="contained">Entertainment</Button>
                <Button className="Botones" sx={{backgroundColor: "#FFFFFF0D", color: "#B6B6B6" }} variant="contained">Energy</Button>
                <Button className="Botones" sx={{ backgroundColor: "#FFFFFF0D", color: "#B6B6B6"  }} variant="contained">Gaming</Button>
                <Button className="Botones" sx={{backgroundColor: "#FFFFFF0D", color: "#B6B6B6"  }} variant="contained">Music</Button>
                <Button className="Botones btn2" sx={{backgroundColor: "#FFFFFF0D", color: "#B6B6B6"  }} variant="contained">See All 12+</Button>

              </Box>
              <Box sx={{ paddingLeft: "20px", marginRight: "20px"}}>
                <Button sx={{ color: "#B6B6B6", border: "1px solid  #B6B6B6", width: "282px", height: "44px", paddingLeft: "20px" }} variant="outlined" startIcon={<Search />}>
                  Search Basic
                </Button>
              </Box>
            </Box>
          </Box>



          <Grid container spacing={2}>
            <Box sx={{ display: "flex", justifyContent: "space-between", flexDirection: "row", }}>
              <Box sx={{ width: "400px", height: "239px", marginTop: "30px", marginLeft: "20px" }}>
                <Typography sx={{ color: "#ECF1F0", fontWeight: "600", fontSize: "36px" }} variant="h5" gutterBottom className='pagination-item'>
                  How To Get Started
                </Typography>
                <Typography sx={{ color: "#B6B6B6", fontWeight: "500", fontSize: "18px", marginTop: "30px" }} variant="caption" display="block" gutterBottom>
                  Simple and easy way to start your investment
                  in cryptocurrency
                </Typography>
                <Button sx={{ backgroundColor: "#0FAE96", width: "190px", height: "56px", borderRadius: "10px", color: "white", marginTop: "50px" }} variant="contained" > Get Started </Button>
              </Box>
              <Box>
                <BoxStarted image="/images/Started.svg" title="Create Your Account" parrafo="Your account and personal identity are guaranteed safe"></BoxStarted>
                <BoxStarted image="/images/Started2.svg" title="Connect Bank Account" parrafo="Connect the bank account to start transactions."></BoxStarted>
                <BoxStarted image="/images/Started3.svg" title="Start Build Portfolio" parrafo="Buy and sell popular currencies and keep track of them."></BoxStarted>
              </Box>

            </Box>

          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12} >

            </Grid>

          </Grid>

          <Grid container spacing={2}>
            <Grid item xs={12} >
              <Box sx={{ display: "flex", backgroundColor: "#FFFFFF05", justifyContent: "space-between", marginBottom: "20px" }}>

                <BoxEnd image="/images/Hero.svg" button="TIPS $ TRINKS" title="How to setup crypto wallet in your account" parrafo="A crypto wallet is a place where you can securely keep your crypto.. "></BoxEnd>
                <BoxEnd image="/images/Hero.svg" button="TIPS $ TRINKS"title="How to setup crypto wallet in your account" parrafo="A crypto wallet is a place where you can securely keep your crypto.. "></BoxEnd>
                <BoxEnd image="/images/Hero.svg" button="TIPS $ TRINKS"title="How to setup crypto wallet in your account" parrafo="A crypto wallet is a place where you can securely keep your crypto.. "></BoxEnd>
                <BoxEnd image="/images/Hero.svg" button="TIPS $ TRINKS"title="How to setup crypto wallet in your account" parrafo="A crypto wallet is a place where you can securely keep your crypto.. "></BoxEnd>

              </Box>
            </Grid>
          </Grid>



        </Box>
      </Box>
     
      
   

  );
}
