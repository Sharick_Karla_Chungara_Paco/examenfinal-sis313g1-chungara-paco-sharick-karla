import { Box, Grid, List, ListItem, ListItemText, Typography } from "@mui/material"
import { Logito } from "./Logito";
import Image from "next/image";

interface propsDetailLinks {
    name: string,
    url: string
}

interface propsLinksFooter {
    title: string,
    links: propsDetailLinks[]
}

const generateLinks = (links: propsDetailLinks[]) => {
    return (
        links.map((e: any, index: number)=> {
            return (
                <ListItem key={index}> 
                    <ListItemText
                        primary={e.name}
                    />
                </ListItem>
            )
        })
    )
}

const LinksFooter = ({ title, links }: propsLinksFooter) => {
    return (
        <Box >
            <Typography variant="h5">{title}</Typography>
            <List>
                {generateLinks(links)}
            </List>
        </Box>
    )
}
export const Footer = () => {
    const elementsLinks: propsLinksFooter[] = [

        {
            title: "About Us",
            links: [
                { name: "About ", url: "#" },
                { name: "Carrers", url: "#" },
                { name: "Blog", url: "#" },
                { name: "Legal & privacy", url: "#" },
            ]
        },
        {
            title: "Services",
            links: [
                { name: "Aplications", url: "#" },
                { name: "Buy Crypto", url: "#" },
                { name: "Tailwind", url: "#" },
                { name: "Affilliate", url: "#" },
                { name: "Institutional Services", url: "#" },
            ]
        },
        {
            title: "Learn",
            links: [
                { name: "What is Cryptocurency?", url: "#" },
                { name: "Crypto Basic", url: "#" },
                { name: "Tips and Tutorials", url: "#" },
                { name: "Market Update", url: "#" },
            ]
        }
    ]
    return (
        <Grid container spacing={2} className="footer">
            <Grid  item xs={3} >
                <Logito />
                <Box >
                    <Image src="/images/Red.svg" alt="" width={20} height={20}></Image>
                    <Image src="/images/Red1.svg" alt="" width={20} height={20}></Image>
                    <Image src="/images/Red2.svg" alt="" width={20} height={20}></Image>
                    <Image src="/images/Red3.svg" alt="" width={20} height={20}></Image>
                </Box>
                <Typography variant="body2">
                    2021 CoinMarketCap. All rights reserved
                </Typography>
            </Grid>
            <Grid item xs={9}>
                <Grid container spacing={2}>
                    {elementsLinks.map((e: any, index: number) => {
                        return (
                            <Grid  key={index} item xs={3}>
                                <LinksFooter title={e.title} links={e.links} />
                            </Grid>
                        )
                    })}
                </Grid>
            </Grid>
        </Grid>
    )
}