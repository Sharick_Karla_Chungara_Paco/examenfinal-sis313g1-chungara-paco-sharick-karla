
import Button from '@mui/material/Button';
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';

import { Card, CardContent, CardMedia, Typography } from "@mui/material";
import React from "react";

interface propsBoxInfo {
    image:string,
    title:string,
    parrafo:string,
}

export const BoxInfo = ({image,title,parrafo}:propsBoxInfo) => {
    return (
        <React.Fragment>
            <Card className="Box-info">
                <CardMedia
                    component="img"
                    image={image}
                    className="Box-media-img-info"
                />
                <CardContent>
                    <Typography sx={{color:"#ECF1F0", fontWeight:"600", fontSize:"20px", paddingBottom:"20px"}} color="text.secondary" gutterBottom>
                        {title}
                    </Typography>
                    
                <Typography sx={{color:"#B6B6B6", fontSize:"16px", paddingLeft:"10px",paddingTop:"20px"}}variant="body2" gutterBottom>
                    {parrafo}
                </Typography>
                <Button sx={{color:"#0FAE96", paddingLeft:"10px", paddingTop:"50px"}}variant="text" endIcon={< ArrowRightAltIcon />}>
                See Explained</Button>
                </CardContent>
            </Card>
        </React.Fragment>
    )
}