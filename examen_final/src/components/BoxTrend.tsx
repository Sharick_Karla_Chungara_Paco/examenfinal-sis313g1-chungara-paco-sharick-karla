import { Box, Button, Typography } from "@mui/material";
import Image from "next/image";
export default function Info(props: { imgUrl: any, title: string, button: any, precio: string, porcentaje: string }) {
    return (
        <Box  sx={{backgroundColor:"#FFFFFF0D" , padding:'10px',paddingLeft:'20px', margin:'10px',width:"297px" ,height:'187px', borderRadius:'18px'}}>
            <Box sx={{display:"flex", marginTop:'10px'}} >
                <Image src={props.imgUrl} alt="" width={50} height={50} />
                <Typography  sx={{color:"#ECF1F0", marginLeft:'10px', marginTop:'8px'}}variant="h6" gutterBottom>
                    {props.title}
                </Typography>
                <Button  sx={{color:"BITCOIN", backgroundColor:"#C6C6C6", widht:'15px',height:'20px', marginLeft:'10px', paddingLeft:'5px' , paddingRight:'5px', marginRight:'20px',  marginTop:'15px', paddingBottom:'10px'}} variant="contained">{props.button}</Button>
                <Image src="/images/flecha.svg" alt="" width={48} height={48}></Image>
            </Box>
            <hr />
            <Box sx={{display:'flex'}}>
                <Box sx={{display:'flex' , flexDirection:'column', paddingTop:'5px'}}>
                <Typography sx={{color:"#ECF1F0"}} variant="h5" gutterBottom>
                    {props.precio}
                </Typography>
                <Typography  sx={{color:"#B6B6B6",fontSize:'18px' }} variant="overline" display="block" gutterBottom>
                    {props.porcentaje}
                </Typography>
                </Box>
               
                <Box sx={{display:'flex', paddingLeft:'20px', paddingTop:'30px' }}>
                <Image src="/images/Porcentaje.svg" alt="" width={100} height={53} ></Image>

                </Box>
              
            </Box>
        </Box>



    )
}