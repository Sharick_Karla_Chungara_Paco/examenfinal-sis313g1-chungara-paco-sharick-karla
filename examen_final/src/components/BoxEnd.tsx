import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Avatar from '@mui/material/Avatar';
import IconButton, { IconButtonProps } from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import { Box, Button } from '@mui/material';
import { title } from 'process';

interface BoxEndProps {
    title: string;
    image: string;
    parrafo: string;
    button:any;
 
    
   

}
export default function BoxEnd({ title, image, parrafo, button}: BoxEndProps) {
    const [expanded, setExpanded] = React.useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    return (
        <Card sx={{ display:"flex", justifyContent:"space-between", flexDirection:"column" ,maxWidth: 300, paddingX: "20px", paddingY: "20px", borderRadius: "20px", height: "100%" ,  backgroundColor:'#FFFFFF05'}}>

            <CardMedia
                component="img"
                height="194"
                image={image}
                alt="Error"
            />
            <CardContent sx={{height:"200px", display:"flex",flexDirection:"column", justifyContent:"space-around"}}>
            <Button  sx={{backgroundColor:'#FFFFFF12', color:'#B6B6B6'}}variant="contained">{button}</Button>
                <Typography variant="h6" sx={{color:'#EAEAEA'}}>
                    {title}
                </Typography>
                <Box sx={{ display: "flex", alignItems: "end" }}>
                    <Box sx={{display:"flex"}}>
                       
                        <Box>
                            <Typography variant='body2' color="#EAEAEA">
                                {parrafo}
                            </Typography>
                          

                        </Box>
                    </Box>
                </Box>
            </CardContent>
           

        </Card>
    );
}
