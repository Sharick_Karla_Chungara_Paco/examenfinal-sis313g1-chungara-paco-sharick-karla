
import Button from '@mui/material/Button';
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';

import { Card, CardContent, CardMedia, Typography } from "@mui/material";
import React from "react";

interface propsBoxStarted{
    image:string,
    title:string,
    parrafo:string,
}

export const BoxStarted = ({image,title,parrafo}:propsBoxStarted) => {
    return (
        <React.Fragment>
            <Card className="Box-started">
                <CardMedia
                    component="img"
                    image={image}
                    className="Box-media-img-started"
                />
                <CardContent>
                    <Typography sx={{color:"#ECF1F0", fontWeight:"600", fontSize:"20px", paddingBottom:"20px"}} color="text.secondary" gutterBottom>
                        {title}
                    </Typography>
                    
                <Typography sx={{color:"#B6B6B6", fontSize:"16px", paddingLeft:"10px",paddingTop:"20px"}}variant="body2" gutterBottom>
                    {parrafo}
                </Typography>
              
                </CardContent>
            </Card>
        </React.Fragment>
    )
}