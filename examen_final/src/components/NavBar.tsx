"use client"
import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
//import MyCard from '@/components/myCard';
//import { ThemeProvider } from '@emotion/react';
//import { themePersonal } from '../helpers/theme';
import { Search } from '@mui/icons-material';
import { Logito } from './Logito';
import LanguageIcon from '@mui/icons-material/Language';

interface Props {
    window?: () => Window;
}

const drawerWidth = 240;
const navItems = ['Home', 'Businesses', 'Trade', 'Market', 'Learn'];

export default function Header(props: Props) {
    const { window } = props;
    const [mobileOpen, setMobileOpen] = React.useState(false);

    const handleDrawerToggle = () => {
        setMobileOpen((prevState) => !prevState);
    };

    const drawer = (

        <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center'}}>
            <Typography variant="h6" sx={{ my: 2 , }}>
            <Logito></Logito>
            </Typography>
            <List>
                {navItems.map((item) => (
                    <ListItem key={item} disablePadding>
                        <ListItemButton sx={{ textAlign: 'center'}}>
                            <ListItemText primary={item} />
                        </ListItemButton>
                    </ListItem>
                ))}
            </List>
        </Box>
    );

    const container = window !== undefined ? () => window().document.body : undefined;

    return (
        //<ThemeProvider theme={themePersonal}>
            <Box sx={{ display: 'flex' }}>
                <CssBaseline />
                <AppBar component="nav">
                    <Toolbar sx={{ display: "flex", justifyContent: "space-around", color: "#ffffff" }}>
                        <Typography
                            variant="h6"
                            component="div"
                        >
                             <Logito></Logito>
                        </Typography>
                        <Box>
                            {navItems.map((item) => (
                                <Button key={item} sx={{ marginRight:'30px' , color:"white" }}>
                                    {item}
                                </Button>
                            ))}
                            <Button sx={{ color: "#ffffff",marginRight:'30px', marginLeft:"150px"}} variant="text" startIcon={<LanguageIcon />}>EN </Button>
                            <Button sx={{backgroundColor:"#0FAE96", width:"120px", height:"50px", borderRadius:"10px"}}variant="contained" >
                               Login
                            </Button>
                        </Box>
                    </Toolbar>
                </AppBar>
                <nav>
                    <Drawer
                        container={container}
                        variant="temporary"
                        open={mobileOpen}
                        onClose={handleDrawerToggle}
                        ModalProps={{
                            keepMounted: true, // Better open performance on mobile.
                        }}
                        sx={{
                            display: { xs: 'block', sm: 'none' },
                            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                        }}
                    >
                        {drawer}
                    </Drawer>
                </nav>
                <Box component="main" sx={{ p: 3 }}>
                    <Toolbar />
                </Box>
            </Box>
        //</ThemeProvider>
    );
}

